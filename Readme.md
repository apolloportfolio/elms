<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Employee Learning Management System

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

Introducing Android-based Learning Management System (LMS) application, designed to provide comprehensive training and development resources for employees in a convenient and accessible manner. System offers a user-friendly interface with easy navigation, enabling learners to access a wide range of learning materials, including videos, presentations, and quizzes. With features like progress tracking and personalized learning paths, employees can easily track their development and tailor their learning experience to their individual needs. Additionally, application allows for seamless integration with existing HR systems, making it an ideal solution for organizations of all sizes. With this Android LMS app, businesses can empower their employees with the tools they need to grow and succeed.


</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

The target user of this Android learning management system would likely be an employee who is looking to improve their skills and knowledge within their current role or prepare for future opportunities within the organization. They may be a recent hire seeking to quickly onboard and learn the ins and outs of their position, or a long-term employee looking to stay up-to-date with industry developments and advancements. They could come from any department or level within the organization and have varying levels of experience and education. The common thread among all users is a desire to learn and grow professionally, and the app provides them with a convenient and effective way to do so.


</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement

	1. Accessibility: With the app, users can access training and development resources at any time and from anywhere, eliminating the need for in-person training sessions and reducing the time and cost associated with travel.
	2. Personalization: The app allows users to customize their learning experience, tailoring their learning path to their individual needs and learning pace. This ensures that each user receives relevant and targeted training.
	3. Progress tracking: The app enables users to track their progress and monitor their learning outcomes, giving them a clear understanding of their strengths and areas for improvement. This helps them to identify areas where they may need additional support or resources.
	4. Cost-effectiveness: The app provides a cost-effective solution for organizations that want to offer training and development resources to their employees. By eliminating the need for in-person training sessions and reducing travel expenses, companies can save money while still providing valuable learning opportunities to their staff.
	5. Integration: The app seamlessly integrates with existing HR systems, making it easy for organizations to manage employee training and development. This streamlines administrative tasks and ensures that employees receive the right training at the right time.
	6. Flexibility: The app provides a flexible learning environment, allowing users to learn at their own pace and on their own schedule. This makes it easier for employees to balance their work and personal lives while still pursuing their professional development goals.


</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

	1. User-friendly interface: The app has a user-friendly interface that makes it easy for users to navigate and access the resources they need.
	2. Learning resources: The app provides a range of learning resources, including videos, presentations, quizzes, and other interactive content that users can access to develop their skills and knowledge.
	3. Personalized learning paths: The app enables users to customize their learning paths, selecting the resources that are most relevant to their roles and interests.
	4. Progress tracking: The app tracks user progress and provides feedback on performance, allowing users to monitor their learning outcomes and adjust their approach as needed.
	5. Social learning: The app enables users to connect with each other, share knowledge and experiences, and collaborate on learning projects.
	6. Gamification: The app incorporates gamification features, such as badges and rewards, to motivate users and encourage participation.
	7. Notifications: The app sends notifications to users to remind them of upcoming training sessions, deadlines, or other important events.
	8. Reporting and analytics: The app generates reports and analytics that enable administrators to track user progress, identify areas of strength and weakness, and make data-driven decisions about training and development initiatives.
	9. Integration with HR systems: The app integrates with existing HR systems, allowing administrators to manage user accounts, track progress, and assign training modules from a centralized platform.

</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

	1. Login screen: This screen allows users to log in to their account using their email address and password.
	2. Home screen: This screen serves as the main hub for the app, displaying a personalized dashboard of training resources and progress tracking information.
	3. Course catalog screen: This screen displays a catalog of available courses, allowing users to browse and select courses based on their interests and professional goals.
	4. Course detail screen: This screen provides detailed information about a selected course, including learning objectives, prerequisites, and course modules.
	5. Learning module screen: This screen displays the content of a selected learning module, such as a video, presentation, or quiz.
	6. Progress tracking screen: This screen shows users their progress through a selected course or module, displaying completed and upcoming activities and assignments.
	7. Social learning screen: This screen allows users to connect with each other, share knowledge and experiences, and collaborate on learning projects.
	8. Notifications screen: This screen displays notifications about upcoming training sessions, deadlines, or other important events.
	9. Profile screen: This screen allows users to view and edit their profile information, including their name, contact information, and job title.
	10. Settings screen: This screen allows users to customize app settings, such as notification preferences and language preferences.
	11. Help and support screen: This screen provides users with access to help and support resources, including FAQs, user guides, and customer support contact information.
	12. Search screen: This screen allows users to search for courses, modules, or other resources within the app.
	13. Feedback screen: This screen enables users to provide feedback about the app, courses, or modules, allowing administrators to make improvements and adjustments based on user feedback.


</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>