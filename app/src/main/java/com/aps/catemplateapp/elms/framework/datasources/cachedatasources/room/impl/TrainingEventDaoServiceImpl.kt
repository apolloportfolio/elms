package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingEventDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingEventDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.mappers.TrainingEventCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity4DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class TrainingEventDaoServiceImpl
@Inject
constructor(
    private val dao: TrainingEventDao,
    private val mapper: TrainingEventCacheMapper,
    private val dateUtil: DateUtil
): TrainingEventDaoService {

    override suspend fun insertOrUpdateEntity(entity: TrainingEvent): TrainingEvent {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,

                entity.name,
                entity.description,

                entity.date,
                entity.time,
            )
                val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: TrainingEvent): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<TrainingEvent>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingEvent? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,

        date: String?,
        time: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                name,
                description,

                date,
                time,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                name,
                description,

                date,
                time,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingEvent>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<TrainingEvent> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<TrainingEvent> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}