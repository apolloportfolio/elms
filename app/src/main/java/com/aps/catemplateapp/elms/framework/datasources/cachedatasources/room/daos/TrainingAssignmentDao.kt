package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingAssignmentCacheEntity
import java.util.*


@Dao
interface TrainingAssignmentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : TrainingAssignmentCacheEntity) : Long

    @Query("SELECT * FROM trainingassignment")
    suspend fun get() : List<TrainingAssignmentCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<TrainingAssignmentCacheEntity>): LongArray

    @Query("SELECT * FROM trainingassignment WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): TrainingAssignmentCacheEntity?

    @Query("DELETE FROM trainingassignment WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM trainingassignment")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM trainingassignment")
    suspend fun getAllEntities(): List<TrainingAssignmentCacheEntity>

    @Query("""
        UPDATE trainingassignment 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        assignedUserId = :assignedUserId,
        title = :title,
        resultsEmail = :resultsEmail,
        picture2URI = :picture2URI,
        assignedToUserId = :assignedToUserId
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        assignedUserId: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): Int

    @Query("DELETE FROM trainingassignment WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM trainingassignment")
    suspend fun searchEntities(): List<TrainingAssignmentCacheEntity>
    
    @Query("SELECT COUNT(*) FROM trainingassignment")
    suspend fun getNumEntities(): Int
}