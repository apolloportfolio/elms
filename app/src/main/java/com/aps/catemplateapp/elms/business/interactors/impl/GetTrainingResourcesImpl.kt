package com.aps.catemplateapp.elms.business.interactors.impl

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingResourceCacheDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingResourceNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.business.interactors.abs.GetTrainingResources
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetTrainingResourcesImpl"
private const val LOG_ME = true

class GetTrainingResourcesImpl
@Inject
constructor(
    private val cacheDataSource: TrainingResourceCacheDataSource,
    private val networkDataSource: TrainingResourceNetworkDataSource,
    private val entityFactory: TrainingResourceFactory
): GetTrainingResources {
    override fun getTrainingResources(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<TrainingResource>?) -> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userEntities3 = safeApiCall(
            dispatcher = Dispatchers.IO,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities3(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<TrainingResource>>(
            response = userEntities3,
            stateEvent = stateEvent,
        ) {
            override suspend fun handleSuccess(resultObj: List<TrainingResource>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetEntities3Constants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getTrainingResources().handleSuccess(): ")


                if (resultObj == null) {
                    ALog.d(TAG, "getTrainingResources(): resultObj == null")
                    message = GetEntities3Constants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getTrainingResources(): resultObj != null")
                    returnViewState.entities3List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetEntities3Constants{
        const val GET_ENTITY_SUCCESS = "Successfully got entities 3."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 3 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 3."
    }
}