package com.aps.catemplateapp.elms.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingAssignmentNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingAssignmentFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingAssignmentNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: TrainingAssignmentFirestoreService
): TrainingAssignmentNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: TrainingAssignment) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<TrainingAssignment>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: TrainingAssignment) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<TrainingAssignment> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: TrainingAssignment): TrainingAssignment? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<TrainingAssignment> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<TrainingAssignment>): List<TrainingAssignment>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): TrainingAssignment? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities2(userId : UserUniqueID) : List<TrainingAssignment>? {
        return firestoreService.getUsersEntities2(userId)
    }
}