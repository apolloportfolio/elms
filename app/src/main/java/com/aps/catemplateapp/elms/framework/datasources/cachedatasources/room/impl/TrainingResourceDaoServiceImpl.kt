package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingResourceDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingResourceDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.mappers.TrainingResourceCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingResourceDaoServiceImpl
@Inject
constructor(
    private val dao: TrainingResourceDao,
    private val mapper: TrainingResourceCacheMapper,
    private val dateUtil: DateUtil
): TrainingResourceDaoService {

    override suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.description,

                entity.ownerID,

                entity.title,
                entity.contentType,
                entity.contentUri,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: TrainingResource): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<TrainingResource>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingResource? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        contentType: String?,
        contentUri: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                description,

                ownerID,

                title,
                contentType,
                contentUri,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                description,

                ownerID,

                title,
                contentType,
                contentUri,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingResource>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<TrainingResource> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<TrainingResource> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}