package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.layouts

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.entities.UserTrainingProgress
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen2
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen3
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen4
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen5
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreenPlaceholder
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenStateEvent

@Composable
internal fun RightPaneNavGraphBuilder(
    suggestedTrainingCourses: List<TrainingCourse>?,
    userTrainingProgress: UserTrainingProgress?,
    navigateToTrainingCourse: (TrainingCourse) -> Unit,
    navigateToTrainingResource: (TrainingResource) -> Unit,
    navigateToTrainingEvent: (TrainingEvent) -> Unit,
    navigateToTrainingAssignment: (TrainingAssignment) -> Unit,

    completeUserProfile: () -> Unit,
    currentlyShownTrainingCourse: TrainingCourse?,
    currentlyShownTrainingResources: List<TrainingResource>?,

    currentlyShownTrainingCourses: List<TrainingCourse>?,
    currentlyShownTrainingEvents: List<TrainingEvent>?,
    currentlyShownTrainingAssignments: List<TrainingAssignment>?,

    onTrainingCoursesSearchQueryUpdate: (String) -> Unit,

    launchStateEvent: (HomeScreenStateEvent) -> Unit,
    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,
    stateEventTracker: StateEventTracker,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    card1CurrentlyShownEntity: TrainingCourse? = null,
    card1ActionOnEntity: (TrainingCourse?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
): NavGraphBuilder.() -> Unit {
    return {
        composable(HomeScreenDestination.DetailsScreen1.route) {
            HomeScreenCompDetailsScreen1(
                completeUserProfile = completeUserProfile,
                currentlyShownTrainingCourse = currentlyShownTrainingCourse,
                navigateToTrainingResource = navigateToTrainingResource,
                currentlyShownTrainingResources = currentlyShownTrainingResources,

                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                entity = card1CurrentlyShownEntity,
                actionOnEntity = card1ActionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
            )
        }
        composable(HomeScreenDestination.DetailsScreen2.route) {
            HomeScreenCompDetailsScreen2()
        }
        composable(HomeScreenDestination.DetailsScreen3.route) {
            HomeScreenCompDetailsScreen3()
        }
        composable(HomeScreenDestination.DetailsScreen4.route) {
            HomeScreenCompDetailsScreen4()
        }
        composable(HomeScreenDestination.DetailsScreen5.route) {
            HomeScreenCompDetailsScreen5()
        }
        composable(HomeScreenDestination.DetailsPlaceholderScreen.route) {
            HomeScreenCompDetailsScreenPlaceholder()
        }
    }
}