package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType02
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.entities.UserTrainingProgress
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingEventFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsELMS
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme


// EmployeeLearningManagementSystemHomeScreenContent
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ELMSHomeScreenContent(
    stateEventTracker: StateEventTracker,
    launchInitStateEvent: () -> Unit,

    suggestedTrainingCourses: List<TrainingCourse>?,
    userTrainingProgress: UserTrainingProgress?,
    navigateToTrainingCourse: (TrainingCourse) -> Unit,
    navigateToTrainingResource: (TrainingResource) -> Unit,
    navigateToTrainingEvent: (TrainingEvent) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.elms_app_background,
        composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        val headerBackgroundColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.primaryVariant,
            MaterialTheme.colors.surface,
        )

        LazyColumnWithPullToRefresh(
            onRefresh = launchInitStateEvent,
            isRefreshing = stateEventTracker.isRefreshing,
        ) {
            if(suggestedTrainingCourses != null) {
                // Suggested training courses
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.suggested_training_courses),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeSmall,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.suggested_training_courses)),
                    )
                }
                itemsIndexed(suggestedTrainingCourses) { index, course ->
                    ListItemType02(
                        index = index,
                        itemTitleString = course.name,
                        itemDescription = course.description,
                        onListItemClick = { navigateToTrainingCourse(course) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            course.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }


            // User's training progress
            if(userTrainingProgress != null) {
                // Assigned courses
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.assigned_training_courses),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeSmall,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.assigned_training_courses)),
                    )
                }
                itemsIndexed(userTrainingProgress.assignedCourses) { index, course ->
                    ListItemType02(
                        index = index,
                        itemTitleString = course.name,
                        itemDescription = course.description,
                        onListItemClick = { navigateToTrainingCourse(course) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            course.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index+2)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }

                // Assigned training events
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.assigned_training_events),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeSmall,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.assigned_training_events)),
                    )
                }
                itemsIndexed(userTrainingProgress.assignedEvents) { index, event ->
                    ListItemType02(
                        index = index,
                        itemTitleString = event.name,
                        itemDescription = event.description,
                        onListItemClick = { navigateToTrainingEvent(event) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            event.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index+4)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }

                // Consumed training events
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.consumed_training_events),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeSmall,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.consumed_training_events)),
                    )
                }
                itemsIndexed(userTrainingProgress.consumedEvents) { index, event ->
                    ListItemType02(
                        index = index,
                        itemTitleString = event.name,
                        itemDescription = event.description,
                        onListItemClick = { navigateToTrainingEvent(event) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            event.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index+5)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }

                // Consumed training resources
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.consumed_training_resources),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeSmall,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.consumed_training_resources)),
                    )
                }
                itemsIndexed(userTrainingProgress.consumedResources) { index, resource ->
                    ListItemType02(
                        index = index,
                        itemTitleString = resource.title,
                        itemDescription = resource.description,
                        onListItemClick = { navigateToTrainingResource(resource) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            resource.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }

        }

    }
}

// Preview =========================================================================================
@Preview
@Composable
private fun ELMSHomeScreenContentPreview() {
    val previewTrainingEvents = TrainingEventFactory.createPreviewEntitiesList()
    val previewTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList()
    val previewTrainingResource = TrainingResourceFactory.createPreviewEntitiesList()

    HomeScreenTheme {
        ELMSHomeScreenContent(
            stateEventTracker = StateEventTracker(),
            launchInitStateEvent = {},

            suggestedTrainingCourses = previewTrainingCourses.subList(0, 2),
            userTrainingProgress = UserTrainingProgress(
                id = null,
                created_at = null,
                updated_at = null,
                userId = UserUniqueID.generateFakeRandomID(),
                consumedResources = previewTrainingResource.subList(0, 2),
                consumedEvents = previewTrainingEvents.subList(0, 2),
                assignedCourses = previewTrainingCourses.subList(2, 4),
                assignedEvents = previewTrainingEvents.subList(2, 4),
            ),
            navigateToTrainingCourse = {},
            navigateToTrainingResource = {},
            navigateToTrainingEvent = {},

            isPreview = true,
        )
    }

}
