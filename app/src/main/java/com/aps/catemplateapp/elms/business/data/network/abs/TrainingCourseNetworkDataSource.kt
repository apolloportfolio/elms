package com.aps.catemplateapp.elms.business.data.network.abs

import android.net.Uri
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.interactors.impl.FirestoreTrainingCourseSearchParameters

interface TrainingCourseNetworkDataSource: StandardNetworkDataSource<TrainingCourse> {
    override suspend fun insertOrUpdateEntity(entity: TrainingCourse): TrainingCourse?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: TrainingCourse)

    override suspend fun insertDeletedEntities(Entities: List<TrainingCourse>)

    override suspend fun deleteDeletedEntity(entity: TrainingCourse)

    override suspend fun getDeletedEntities(): List<TrainingCourse>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: TrainingCourse): TrainingCourse?

    override suspend fun getAllEntities(): List<TrainingCourse>

    override suspend fun insertOrUpdateEntities(Entities: List<TrainingCourse>): List<TrainingCourse>?

    override suspend fun getEntityById(id: UniqueID): TrainingCourse?

    suspend fun searchEntities(
        searchParameters : FirestoreTrainingCourseSearchParameters
    ) : List<TrainingCourse>?

    suspend fun getUsersEntites1(userID: UserUniqueID) : List<TrainingCourse>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : TrainingCourse,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}