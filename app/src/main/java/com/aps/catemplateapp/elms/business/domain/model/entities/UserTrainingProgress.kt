package com.aps.catemplateapp.elms.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import kotlinx.parcelize.Parcelize
import java.io.Serializable

// Entity5
@Parcelize
data class UserTrainingProgress(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,
    val userId: UserUniqueID,
    val consumedResources: List<TrainingResource>,
    val consumedEvents: List<TrainingEvent>,
    val assignedCourses: List<TrainingCourse>,
    val assignedEvents: List<TrainingEvent>
): Parcelable, Serializable {

}
