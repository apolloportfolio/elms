package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingEventFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingEventFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingEventFirestoreEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TrainingEventFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: TrainingEventFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): TrainingEventFirestoreService,
    BasicFirestoreServiceImpl<TrainingEvent, TrainingEventFirestoreEntity, TrainingEventFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: TrainingEvent, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: TrainingEvent): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: TrainingEventFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: TrainingEvent, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: TrainingEventFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<TrainingEventFirestoreEntity> {
        return TrainingEventFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: TrainingEventFirestoreEntity): UniqueID? {
        return entity.id
    }

    companion object {
        const val TAG = "TrainingEventFirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity4"
        const val DELETES_COLLECTION_NAME = "entity4_d"
    }
}