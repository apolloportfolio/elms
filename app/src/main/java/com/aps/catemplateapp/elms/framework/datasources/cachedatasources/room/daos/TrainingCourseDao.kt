package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingCourseCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface TrainingCourseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : TrainingCourseCacheEntity) : Long

    @Query("SELECT * FROM trainingcourse")
    suspend fun get() : List<TrainingCourseCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<TrainingCourseCacheEntity>): LongArray

    @Query("SELECT * FROM trainingcourse WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): TrainingCourseCacheEntity?

    @Query("DELETE FROM trainingcourse WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM trainingcourse")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM trainingcourse")
    suspend fun getAllEntities(): List<TrainingCourseCacheEntity>

    @Query(
        """
        UPDATE trainingcourse 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        switch1 = :switch1,
        switch2 = :switch2,
        switch3 = :switch3,
        switch4 = :switch4,
        switch5 = :switch5,
        switch6 = :switch6,
        switch7 = :switch7,
        tags = :tags,
        trainingResources = :trainingResources
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,

        tags: String?,
        trainingResources: String?
    ): Int

    @Query("DELETE FROM trainingcourse WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM trainingcourse")
    suspend fun searchEntities(): List<TrainingCourseCacheEntity>
    
    @Query("SELECT COUNT(*) FROM trainingcourse")
    suspend fun getNumEntities(): Int
}