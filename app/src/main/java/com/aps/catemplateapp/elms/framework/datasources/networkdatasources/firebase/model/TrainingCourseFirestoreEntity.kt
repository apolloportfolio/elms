package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.firebase.firestore.GeoPoint
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

private const val TAG = "TrainingCourseFirestoreEntity"
private const val LOG_ME = true

@Entity(tableName = "entities1")
data class TrainingCourseFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("lattitude")
    @Expose
    var lattitude : Double?,

    @SerializedName("longitude")
    @Expose
    var longitude: Double?,

    @SerializedName("geoLocation")
    @Expose
    var geoLocation: GeoPoint?,

    @SerializedName("firestoreGeoLocation")
    @Expose
    var firestoreGeoLocation: Double?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description : String?,

    @SerializedName("city")
    @Expose
    var city : String?,

    @SerializedName("keywords")
    @Expose
    var keywords : List<String>? = null,

    @SerializedName("ownerID")
    @Expose
    var ownerID: UserUniqueID?,

    @SerializedName("name")
    @Expose
    var name : String?,

    @SerializedName("switch1")
    @Expose
    var switch1 : Boolean?,

    @SerializedName("switch2")
    @Expose
    var switch2 : Boolean?,

    @SerializedName("switch3")
    @Expose
    var switch3 : Boolean?,

    @SerializedName("switch4")
    @Expose
    var switch4 : Boolean?,

    @SerializedName("switch5")
    @Expose
    var switch5 : Boolean?,

    @SerializedName("switch6")
    @Expose
    var switch6 : Boolean?,

    @SerializedName("switch7")
    @Expose
    var switch7 : Boolean?,

    @SerializedName("tags")
    @Expose
    var tags : String?,

    @SerializedName("trainingResources")
    @Expose
    var trainingResources : String?,

    ) {

    // Method generates keywords for searching entities in Firestore
    // https://medium.com/flobiz-blog/full-text-search-with-firestore-on-android-622af6ca5410
    fun generateKeywords(): TrainingCourseFirestoreEntity {
        val methodName: String = "generateKeywords"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val generatedKeywords = mutableListOf<String>()

            this.city?.let {
                for (i in 0 until city!!.length) {
                    for (j in (i+1)..city!!.length) {
                        generatedKeywords.add(city!!.slice(i until j).toLowerCase() + " ")
                    }
                }
            }

            keywords = generatedKeywords
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return this
    }

    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}