package com.aps.catemplateapp.elms.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingResourceNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingResourceFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingResourceNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: TrainingResourceFirestoreService
): TrainingResourceNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: TrainingResource) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<TrainingResource>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: TrainingResource) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<TrainingResource> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: TrainingResource): TrainingResource? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<TrainingResource> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<TrainingResource>): List<TrainingResource>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): TrainingResource? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<TrainingResource>? {
        return firestoreService.getUsersEntities3(userId)
    }
}