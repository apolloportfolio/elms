package com.aps.catemplateapp.elms.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse

interface TrainingCourseCacheDataSource: StandardCacheDataSource<TrainingCourse> {
    override suspend fun insertEntity(entity: TrainingCourse): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<TrainingCourse>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        tags: String?,
        trainingResources: String?
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingCourse>

    override suspend fun getAllEntities(): List<TrainingCourse>

    override suspend fun getEntityById(id: UniqueID?): TrainingCourse?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<TrainingCourse>): LongArray
}