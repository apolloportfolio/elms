package com.aps.catemplateapp.elms.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingAssignmentFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        name: String?,
        description : String?,

        assignedUserId: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): TrainingAssignment {
        return TrainingAssignment(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            name,
            description,
            assignedUserId,

            title,
            resultsEmail,
            picture2URI,
            assignedToUserId,
        )
    }

    fun createEntitiesList(numEntities: Int): List<TrainingAssignment> {
        val list: ArrayList<TrainingAssignment> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): TrainingAssignment {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<TrainingAssignment> {
            return arrayListOf(
                TrainingAssignment(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test01",
                    description = "Enhance your leadership skills with this comprehensive training program designed for aspiring managers.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Managerial Leadership Training",
                    resultsEmail = "example1@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test02",
                    description = "Learn effective communication strategies for better collaboration and productivity in your workplace.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Effective Communication Workshop",
                    resultsEmail = "example2@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test03",
                    description = "Unlock the secrets of successful project management and drive your projects to success.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Project Management Masterclass",
                    resultsEmail = "example3@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test04",
                    description = "Discover practical strategies for building and leading high-performing teams in your organization.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Building High-Performance Teams",
                    resultsEmail = "example4@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test05",
                    description = "Develop your analytical skills and make data-driven decisions with our hands-on analytics training.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Analytics Training Bootcamp",
                    resultsEmail = "example5@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test06",
                    description = "Gain a deeper understanding of customer behavior and create effective marketing strategies.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Customer Behavior Analysis Workshop",
                    resultsEmail = "example6@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test07",
                    description = "Learn the latest trends in digital marketing and develop strategies to stay ahead of the curve.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Digital Marketing Trends Seminar",
                    resultsEmail = "example7@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test08",
                    description = "Harness the power of artificial intelligence to optimize business processes and drive growth.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "AI for Business Leaders Workshop",
                    resultsEmail = "example8@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test09",
                    description = "Improve your time management skills and achieve better work-life balance with our practical tips.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Time Management Strategies Seminar",
                    resultsEmail = "example9@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                ),
                TrainingAssignment(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    name = "test10",
                    description = "Explore the fundamentals of cybersecurity and protect your organization from cyber threats.",
                    assignedUserId = UserUniqueID("666", "666"),
                    title = "Cybersecurity Basics Workshop",
                    resultsEmail = "example10@example.com",
                    picture2URI = "2",
                    assignedToUserId = UserUniqueID("777", "777")
                )
            )
        }


    }
}