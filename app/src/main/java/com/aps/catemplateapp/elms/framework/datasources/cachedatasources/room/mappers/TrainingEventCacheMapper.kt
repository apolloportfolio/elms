package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingEventCacheEntity
import javax.inject.Inject

class TrainingEventCacheMapper
@Inject
constructor() : EntityMapper<TrainingEventCacheEntity, TrainingEvent> {
    override fun mapFromEntity(entity: TrainingEventCacheEntity): TrainingEvent {
        return TrainingEvent(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,

            entity.date,
            entity.time,
        )
    }

    override fun mapToEntity(domainModel: TrainingEvent): TrainingEventCacheEntity {
        return TrainingEventCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,

            domainModel.date,
            domainModel.time,
        )
    }

    override fun mapFromEntityList(entities : List<TrainingEventCacheEntity>) : List<TrainingEvent> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingEvent>): List<TrainingEventCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}