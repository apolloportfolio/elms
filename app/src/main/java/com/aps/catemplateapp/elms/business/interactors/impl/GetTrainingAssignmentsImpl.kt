package com.aps.catemplateapp.elms.business.interactors.impl

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingAssignmentCacheDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingAssignmentNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingAssignmentFactory
import com.aps.catemplateapp.elms.business.interactors.abs.GetTrainingAssignments
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetTrainingAssignmentsImpl"
private const val LOG_ME = true

class GetTrainingAssignmentsImpl
@Inject
constructor(
    private val cacheDataSource: TrainingAssignmentCacheDataSource,
    private val networkDataSource: TrainingAssignmentNetworkDataSource,
    private val entityFactory: TrainingAssignmentFactory
): GetTrainingAssignments {
    override fun getTrainingAssignments(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<TrainingAssignment>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userTrips = safeApiCall(
            dispatcher = Dispatchers.IO,
            onErrorAction = onErrorAction,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities2(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<TrainingAssignment>>(
            response = userTrips,
            stateEvent = stateEvent,
        ) {
            override suspend fun handleSuccess(resultObj: List<TrainingAssignment>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetUsersTripsImplConstants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getTrainingAssignments().handleSuccess(): ")


                if(resultObj == null){
                    ALog.d(TAG, "getTrainingAssignments(): resultObj == null")
                    message = GetUsersTripsImplConstants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getTrainingAssignments(): resultObj != null")
                    returnViewState.entities2List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetUsersTripsImplConstants{
        const val GET_ENTITY_SUCCESS = "Successfully entities 2."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 2 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 2."
    }
}
