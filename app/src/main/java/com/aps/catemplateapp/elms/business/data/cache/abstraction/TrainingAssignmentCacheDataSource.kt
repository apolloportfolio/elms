package com.aps.catemplateapp.elms.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment

interface TrainingAssignmentCacheDataSource: StandardCacheDataSource<TrainingAssignment> {
    override suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment?

    override suspend fun insertEntity(entity: TrainingAssignment): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<TrainingAssignment>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        assignedUserId: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingAssignment>

    override suspend fun getAllEntities(): List<TrainingAssignment>

    override suspend fun getEntityById(id: UniqueID?): TrainingAssignment?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<TrainingAssignment>): LongArray
}