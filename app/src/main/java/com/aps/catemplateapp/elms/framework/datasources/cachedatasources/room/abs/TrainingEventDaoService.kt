package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent

interface TrainingEventDaoService {
    suspend fun insertOrUpdateEntity(entity: TrainingEvent): TrainingEvent

    suspend fun insertEntity(entity: TrainingEvent): Long

    suspend fun insertEntities(Entities: List<TrainingEvent>): LongArray

    suspend fun getEntityById(id: UniqueID?): TrainingEvent?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,

        date: String?,
        time: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<TrainingEvent>): Int

    suspend fun searchEntities(): List<TrainingEvent>

    suspend fun getAllEntities(): List<TrainingEvent>

    suspend fun getNumEntities(): Int
}