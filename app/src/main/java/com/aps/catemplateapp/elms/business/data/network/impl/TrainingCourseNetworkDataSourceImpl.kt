package com.aps.catemplateapp.elms.business.data.network.impl

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingCourseNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.interactors.impl.FirestoreTrainingCourseSearchParameters
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingCourseFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TrainingCourseNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: TrainingCourseFirestoreService
): TrainingCourseNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: TrainingCourse): TrainingCourse? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: TrainingCourse) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<TrainingCourse>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: TrainingCourse) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<TrainingCourse> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: TrainingCourse): TrainingCourse? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<TrainingCourse> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<TrainingCourse>): List<TrainingCourse>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): TrainingCourse? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreTrainingCourseSearchParameters
    ) : List<TrainingCourse>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUsersEntites1(userID: UserUniqueID) : List<TrainingCourse>? {
        return firestoreService.getUsersEntities1(userID)
    }

    override suspend fun uploadEntity1PhotoToFirestore(
        entity : TrainingCourse,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
