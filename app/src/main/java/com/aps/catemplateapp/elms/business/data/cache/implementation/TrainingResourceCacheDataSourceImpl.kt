package com.aps.catemplateapp.elms.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingResourceCacheDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingResourceDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingResourceCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: TrainingResourceDaoService
): TrainingResourceCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: TrainingResource): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingResource>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        contentType: String?,
        contentUri: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,

            title,
            contentType,
            contentUri,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingResource> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<TrainingResource> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingResource? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<TrainingResource>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}