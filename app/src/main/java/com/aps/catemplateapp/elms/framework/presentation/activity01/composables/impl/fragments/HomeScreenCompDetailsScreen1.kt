package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompDetailsScreen1"
private const val LOG_ME = true
const val HomeScreenCompDetailsScreen1TestTag = TAG
const val HomeScreenCompoDetailsScreen1CityTestTag = "HomeScreenCompDetailsScreen1TestTag"
const val HomeScreenCompDetailsScreen1ActionButtonTestTag = "HomeScreenCompDetailsScreen1ActionButtonTestTag"

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompDetailsScreen1(
    completeUserProfile: () -> Unit,
    currentlyShownTrainingCourse: TrainingCourse?,
    navigateToTrainingResource: (TrainingResource) -> Unit,
    currentlyShownTrainingResources: List<TrainingResource>?,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    entity: TrainingCourse?,
    actionOnEntity: (TrainingCourse?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    val permissionsRequiredInFragment = mutableSetOf<String>()

    val scope = rememberCoroutineScope()            // For bottom sheet if implemented.
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetTrainingResourcesOfTrainingCourse(
                    currentlyShownTrainingCourse,
                    completeUserProfile,
                )
            )
        } else {
            if(LOG_ME)ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = {},
        onSwipeRight = {},
        onSwipeUp = {},
        onSwipeDown = {},
        onSwipeRightFromComposableSide = {},
        onSwipeLeftFromComposableSide = {},
        onSwipeDownFromComposableSide = {},
        onSwipeUpFromComposableSide = {},
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = null,
        floatingActionButtonOnClick = {},
        floatingActionButtonContentDescription = null,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = {},
        content = {
            ELMSCourseDetailsScreenContent(
                stateEventTracker = stateEventTracker,
                launchStateEvent = launchStateEvent,
                refreshList = launchInitStateEvent,

                completeUserProfile = completeUserProfile,
                currentlyShownTrainingCourse = currentlyShownTrainingCourse,
                navigateToTrainingResource = navigateToTrainingResource,
                currentlyShownTrainingResources = currentlyShownTrainingResources,
                isPreview = isPreview,
            )
        },
        isPreview = isPreview,
    )
}

//========================================================================================
@Preview
@Composable
fun HomeScreenComposableDetailsScreen1Preview() {
    val previewEntity = TrainingCourseFactory.createPreviewEntitiesList()[0]
    val navigateToProfileScreen = {}
    val actionOnEntity: (TrainingCourse?, ()-> Unit) -> Unit = { _, _ -> }
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompDetailsScreen1(
            completeUserProfile = {},
            currentlyShownTrainingCourse = TrainingCourseFactory.createPreviewEntitiesList()[0],
            navigateToTrainingResource = {},
            currentlyShownTrainingResources = TrainingResourceFactory.createPreviewEntitiesList(),

            activity = null,
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            launchStateEvent = {},
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            entity = previewEntity,
            actionOnEntity = actionOnEntity,
            navigateToProfileScreen = navigateToProfileScreen,
        )
    }
}