package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingAssignmentFirestoreEntity
import javax.inject.Inject

class TrainingAssignmentFirestoreMapper
@Inject
constructor() : EntityMapper<TrainingAssignmentFirestoreEntity, TrainingAssignment> {
    override fun mapFromEntity(entity: TrainingAssignmentFirestoreEntity): TrainingAssignment {
        return TrainingAssignment(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.assignedUserId,

            entity.title,
            entity.resultsEmail,
            entity.picture2URI,
            entity.assignedToUserId,
        )
    }

    override fun mapToEntity(domainModel: TrainingAssignment): TrainingAssignmentFirestoreEntity {
        return TrainingAssignmentFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
            domainModel.assignedUserId,

            domainModel.title,
            domainModel.resultsEmail,
            domainModel.picture2URI,
            domainModel.assignedToUserId,
        )
    }

    override fun mapFromEntityList(entities : List<TrainingAssignmentFirestoreEntity>) : List<TrainingAssignment> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingAssignment>): List<TrainingAssignmentFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}