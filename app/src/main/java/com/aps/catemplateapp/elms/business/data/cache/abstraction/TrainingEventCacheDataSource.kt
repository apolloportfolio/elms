package com.aps.catemplateapp.elms.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent

interface TrainingEventCacheDataSource: StandardCacheDataSource<TrainingEvent> {
    override suspend fun insertEntity(entity: TrainingEvent): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<TrainingEvent>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,

        date: String?,
        time: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingEvent>

    override suspend fun getAllEntities(): List<TrainingEvent>

    override suspend fun getEntityById(id: UniqueID?): TrainingEvent?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<TrainingEvent>): LongArray
}