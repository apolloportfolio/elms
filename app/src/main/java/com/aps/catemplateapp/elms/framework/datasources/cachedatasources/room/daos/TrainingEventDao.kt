package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingEventCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface TrainingEventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : TrainingEventCacheEntity) : Long

    @Query("SELECT * FROM trainingevent")
    suspend fun get() : List<TrainingEventCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<TrainingEventCacheEntity>): LongArray

    @Query("SELECT * FROM trainingevent WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): TrainingEventCacheEntity?

    @Query("DELETE FROM trainingevent WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM trainingevent")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM trainingevent")
    suspend fun getAllEntities(): List<TrainingEventCacheEntity>

    @Query(
        """
        UPDATE trainingevent 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        date = :date,
        time = :time
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        name: String?,
        description : String?,

        date: String?,
        time: String?,
    ): Int

    @Query("DELETE FROM trainingevent WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM trainingevent")
    suspend fun searchEntities(): List<TrainingEventCacheEntity>
    
    @Query("SELECT COUNT(*) FROM trainingevent")
    suspend fun getNumEntities(): Int
}