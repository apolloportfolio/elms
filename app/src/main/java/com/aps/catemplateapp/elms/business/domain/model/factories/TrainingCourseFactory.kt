package com.aps.catemplateapp.elms.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingCourseFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        tags: String?,
        trainingResources: String?
    ): TrainingCourse {
        return TrainingCourse(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,

            city,

            ownerID,

            name,

            tags = tags,
            trainingResources = trainingResources,
        )
    }

    fun createEntitiesList(numEntities: Int): List<TrainingCourse> {
        val list: ArrayList<TrainingCourse> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): TrainingCourse {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }
    companion object {
        fun createPreviewEntitiesList(): List<TrainingCourse> {
            val tagsList = listOf("communication", "leadership", "teamwork")
            val trainingResourcesList = listOf("test1", "test2", "test3", "test4", "test5")

            return arrayListOf(
                TrainingCourse(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "A hands-on training course for aspiring baristas, covering espresso preparation, milk steaming, latte art techniques, and customer service skills. Gain practical experience in a simulated cafe environment.",
                    name = "Barista Training Program",city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(0, 2).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Master the art of cocktail making with this interactive training course. Learn classic cocktail recipes, mixology techniques, and bartending etiquette from industry professionals.",
                    name = "Mixology Training Workshop",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(2, 4).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Enhance your culinary skills with this comprehensive cooking course. Learn knife techniques, cooking methods, flavor pairing, and plating presentation to create restaurant-quality dishes.",
                    name = "Gourmet Cooking Training Program",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(4, 5).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Unlock the secrets of effective sales techniques with this intensive training course. Learn persuasion strategies, objection handling, and relationship building to boost your sales performance.",
                    name = "Sales Mastery Training Program",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(0, 2).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Gain practical skills in project management with this comprehensive training course. Learn project planning, budgeting, risk management, and team leadership to deliver successful projects.",
                    name = "Project Management Certification Training",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(2, 4).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Develop your leadership abilities with this interactive training program. Learn effective communication, decision-making, conflict resolution, and team motivation strategies to excel as a leader.",
                    name = "Leadership Development Training",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(4, 5).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Improve your customer service skills with this engaging training course. Learn to handle customer inquiries, complaints, and feedback effectively to enhance customer satisfaction.",
                    name = "Customer Service Excellence Training",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(0, 2).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Become proficient in digital marketing with this comprehensive training program. Learn SEO, social media marketing, email marketing, and analytics to drive online business growth.",
                    name = "Digital Marketing Training Bootcamp",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(2, 4).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Master the art of negotiation with this practical training course. Learn negotiation tactics, communication strategies, and conflict resolution techniques to achieve win-win outcomes.",
                    name = "Negotiation Skills Training Workshop",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(4, 5).joinToString(",")
                ),
                TrainingCourse(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Enhance your knowledge of cybersecurity with this specialized training program. Learn threat detection, incident response, ethical hacking, and risk management to protect digital assets.",
                    name = "Cybersecurity Training and Certification",city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    tags = tagsList.joinToString(","),
                    trainingResources = trainingResourcesList.subList(0, 2).joinToString(",")
                )
            )
        }

    }
}