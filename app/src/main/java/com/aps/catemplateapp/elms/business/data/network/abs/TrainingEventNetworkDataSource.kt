package com.aps.catemplateapp.elms.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent

interface TrainingEventNetworkDataSource: StandardNetworkDataSource<TrainingEvent> {
    override suspend fun insertOrUpdateEntity(entity: TrainingEvent): TrainingEvent?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: TrainingEvent)

    override suspend fun insertDeletedEntities(Entities: List<TrainingEvent>)

    override suspend fun deleteDeletedEntity(entity: TrainingEvent)

    override suspend fun getDeletedEntities(): List<TrainingEvent>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: TrainingEvent): TrainingEvent?

    override suspend fun getAllEntities(): List<TrainingEvent>

    override suspend fun insertOrUpdateEntities(Entities: List<TrainingEvent>): List<TrainingEvent>?

    override suspend fun getEntityById(id: UniqueID): TrainingEvent?
}