package com.aps.catemplateapp.elms.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.elms.ELMSViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.elms.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object ELMSViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("ELMSViewModelFactory")
    fun provideELMSViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        trainingCourseFactory: TrainingCourseFactory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return ELMSViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            trainingCourseFactory = trainingCourseFactory,
            editor = editor,
        )
    }
}