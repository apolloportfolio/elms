package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingEventFirestoreEntity
import javax.inject.Inject

class TrainingEventFirestoreMapper
@Inject
constructor() : EntityMapper<TrainingEventFirestoreEntity, TrainingEvent> {
    override fun mapFromEntity(entity: TrainingEventFirestoreEntity): TrainingEvent {
        val trainingEvent = TrainingEvent(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,

            entity.date,
            entity.time,
        )
        if(entity.geoLocation != null){
            trainingEvent.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return trainingEvent
    }

    override fun mapToEntity(domainModel: TrainingEvent): TrainingEventFirestoreEntity {
        return TrainingEventFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,

            domainModel.date,
            domainModel.time,
        )
    }

    override fun mapFromEntityList(entities : List<TrainingEventFirestoreEntity>) : List<TrainingEvent> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingEvent>): List<TrainingEventFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}