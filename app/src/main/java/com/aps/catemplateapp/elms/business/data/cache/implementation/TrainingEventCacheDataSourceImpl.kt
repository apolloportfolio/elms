package com.aps.catemplateapp.elms.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingEventCacheDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingEventDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingEventCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: TrainingEventDaoService
): TrainingEventCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: TrainingEvent): TrainingEvent? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: TrainingEvent): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingEvent>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,

        date: String?,
        time: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,

            date,
            time,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingEvent> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<TrainingEvent> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingEvent? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<TrainingEvent>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}