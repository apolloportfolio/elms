package com.aps.catemplateapp.elms.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingCourseCacheDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingCourseDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingCourseCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: TrainingCourseDaoService
): TrainingCourseCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: TrainingCourse): TrainingCourse? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: TrainingCourse): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingCourse>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        tags: String?,
        trainingResources: String?
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            city,
            ownerID,
            name,

            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,

            tags,
            trainingResources,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingCourse> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<TrainingCourse> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingCourse? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<TrainingCourse>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}