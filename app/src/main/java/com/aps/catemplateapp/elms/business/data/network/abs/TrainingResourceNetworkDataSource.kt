package com.aps.catemplateapp.elms.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource

interface TrainingResourceNetworkDataSource: StandardNetworkDataSource<TrainingResource> {
    override suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: TrainingResource)

    override suspend fun insertDeletedEntities(Entities: List<TrainingResource>)

    override suspend fun deleteDeletedEntity(entity: TrainingResource)

    override suspend fun getDeletedEntities(): List<TrainingResource>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: TrainingResource): TrainingResource?

    override suspend fun getAllEntities(): List<TrainingResource>

    override suspend fun insertOrUpdateEntities(Entities: List<TrainingResource>): List<TrainingResource>?

    override suspend fun getEntityById(id: UniqueID): TrainingResource?

    suspend fun getUsersEntities3(userId: UserUniqueID): List<TrainingResource>?
}