package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingAssignmentFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingEventFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompFragment3"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment3(
    currentlyShownTrainingCourses: List<TrainingCourse>?,
    currentlyShownTrainingEvents: List<TrainingEvent>?,
    currentlyShownTrainingAssignments: List<TrainingAssignment>?,
    onCourseClick: (TrainingCourse) -> Unit,
    onEventClick: (TrainingEvent) -> Unit,
    onAssignmentClick: (TrainingAssignment) -> Unit,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    entities: List<TrainingResource>?,
    onListItemClick: (TrainingResource) -> Unit,
    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},
    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment3BottomSheetActions,

    isPreview: Boolean = false,
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )

    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.w(TAG, "ShowContent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetEntities3
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "ShowContent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = mutableSetOf(),
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = HomeScreenCompFragment3BottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            ELMSProgressTrackingScreenContent(
                stateEventTracker = stateEventTracker,
                launchInitStateEvent = launchInitStateEvent,

                currentlyShownTrainingCourses = currentlyShownTrainingCourses,
                currentlyShownTrainingEvents = currentlyShownTrainingEvents,
                currentlyShownTrainingAssignments = currentlyShownTrainingAssignments,
                onCourseClick = onCourseClick,
                onEventClick = onEventClick,
                onAssignmentClick = onAssignmentClick,

                isPreview = isPreview,
            )
        }
    )
}

// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment3Preview(
    @PreviewParameter(
        HomeScreenComposableFragment3ParamsProvider::class
    ) params: HomeScreenComposableFragment3Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment3BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompFragment3(
            currentlyShownTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList(),
            currentlyShownTrainingEvents = TrainingEventFactory.createPreviewEntitiesList(),
            currentlyShownTrainingAssignments = TrainingAssignmentFactory.createPreviewEntitiesList(),
            onCourseClick = {},
            onEventClick = {},
            onAssignmentClick = {},

            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            entities = params.entities,
            onListItemClick = params.onListItemClick,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            bottomSheetActions = bottomSheetActions,
            isPreview = true,
        )
    }
}

class HomeScreenComposableFragment3ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment3Params> {
    override val values: Sequence<HomeScreenComposableFragment3Params> = sequenceOf(
        HomeScreenComposableFragment3Params(
            deviceLocation = DeviceLocation(
                locationPermissionGranted = true,
                location = Location("Lublin").apply {
                    latitude = 51.2465
                    longitude = 22.5684
                }
            ),
            showProfileStatusBar = true,
            finishVerification = {},
            entities = TrainingResourceFactory.createPreviewEntitiesList(),
            onListItemClick = {},
            onSwipeLeft = {},
            onSwipeRight = {},
            onSwipeUp = {},
            onSwipeDown = {},
            floatingActionButtonDrawableId = R.drawable.ic_baseline_add_24,
            floatingActionButtonOnClick = { },
            floatingActionButtonContentDescription = "FAB 3",
        )
    )
}

data class HomeScreenComposableFragment3Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val entities: List<TrainingResource>?,
    val onListItemClick: (TrainingResource) -> Unit,
    val onSwipeLeft: () -> Unit,
    val onSwipeRight: () -> Unit,
    val onSwipeUp: () -> Unit,
    val onSwipeDown: () -> Unit,
    val floatingActionButtonDrawableId: Int?,
    val floatingActionButtonOnClick: (() -> Unit)?,
    val floatingActionButtonContentDescription: String?,
)