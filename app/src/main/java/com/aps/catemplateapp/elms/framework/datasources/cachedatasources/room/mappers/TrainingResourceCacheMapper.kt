package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingResourceCacheEntity
import javax.inject.Inject

class TrainingResourceCacheMapper
@Inject
constructor() : EntityMapper<TrainingResourceCacheEntity, TrainingResource> {
    override fun mapFromEntity(entity: TrainingResourceCacheEntity): TrainingResource {
        return TrainingResource(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.title,
            entity.contentType,
            entity.contentUri,
        )
    }

    override fun mapToEntity(domainModel: TrainingResource): TrainingResourceCacheEntity {
        return TrainingResourceCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.description,

            domainModel.ownerID,

            domainModel.title,
            domainModel.contentType,
            domainModel.contentUri,
        )
    }

    override fun mapFromEntityList(entities : List<TrainingResourceCacheEntity>) : List<TrainingResource> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingResource>): List<TrainingResourceCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}