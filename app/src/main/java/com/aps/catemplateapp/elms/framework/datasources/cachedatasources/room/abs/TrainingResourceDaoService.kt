package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource

interface TrainingResourceDaoService {
    suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource

    suspend fun insertEntity(entity: TrainingResource): Long

    suspend fun insertEntities(Entities: List<TrainingResource>): LongArray

    suspend fun getEntityById(id: UniqueID?): TrainingResource?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        contentType: String?,
        contentUri: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<TrainingResource>): Int

    suspend fun searchEntities(): List<TrainingResource>

    suspend fun getAllEntities(): List<TrainingResource>

    suspend fun getNumEntities(): Int
}