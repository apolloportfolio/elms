package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingResourceCacheEntity
import java.util.*


@Dao
interface TrainingResourceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : TrainingResourceCacheEntity) : Long

    @Query("SELECT * FROM trainingresource")
    suspend fun get() : List<TrainingResourceCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<TrainingResourceCacheEntity>): LongArray

    @Query("SELECT * FROM trainingresource WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): TrainingResourceCacheEntity?

    @Query("DELETE FROM trainingresource WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM trainingresource")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM trainingresource")
    suspend fun getAllEntities(): List<TrainingResourceCacheEntity>

    @Query("""
        UPDATE trainingresource 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        description = :description,
        ownerId = :ownerID,
        description = :title,
        description = :contentType,
        description = :contentUri
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        contentType: String?,
        contentUri: String?,
    ): Int

    @Query("DELETE FROM trainingresource WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM trainingresource")
    suspend fun searchEntities(): List<TrainingResourceCacheEntity>
    
    @Query("SELECT COUNT(*) FROM trainingresource")
    suspend fun getNumEntities(): Int
}