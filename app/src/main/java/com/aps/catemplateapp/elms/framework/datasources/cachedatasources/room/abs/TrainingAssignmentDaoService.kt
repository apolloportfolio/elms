package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment

interface TrainingAssignmentDaoService {
    suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment

    suspend fun insertEntity(entity: TrainingAssignment): Long

    suspend fun insertEntities(Entities: List<TrainingAssignment>): LongArray

    suspend fun getEntityById(id: UniqueID?): TrainingAssignment?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        assignedUserId: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<TrainingAssignment>): Int

    suspend fun searchEntities(): List<TrainingAssignment>

    suspend fun getAllEntities(): List<TrainingAssignment>

    suspend fun getNumEntities(): Int
}