package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingResourceFirestoreEntity
import javax.inject.Inject

class TrainingResourceFirestoreMapper
@Inject
constructor() : EntityMapper<TrainingResourceFirestoreEntity, TrainingResource> {
    override fun mapFromEntity(entity: TrainingResourceFirestoreEntity): TrainingResource {
        return TrainingResource(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.title,
            entity.contentType,
            entity.contentUri,
        )
    }

    override fun mapToEntity(domainModel: TrainingResource): TrainingResourceFirestoreEntity {
        return TrainingResourceFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.description,
            domainModel.ownerID,

            domainModel.title,
            domainModel.contentType,
            domainModel.contentUri,
        )
    }

    override fun mapFromEntityList(entities : List<TrainingResourceFirestoreEntity>) : List<TrainingResource> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingResource>): List<TrainingResourceFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}