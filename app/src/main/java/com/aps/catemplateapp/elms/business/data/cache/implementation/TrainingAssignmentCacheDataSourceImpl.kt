package com.aps.catemplateapp.elms.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingAssignmentCacheDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingAssignmentDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingAssignmentCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: TrainingAssignmentDaoService
): TrainingAssignmentCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: TrainingAssignment): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingAssignment>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        assignedUserId: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            name,
            description,

            assignedUserId,

            title,
            resultsEmail,
            picture2URI,
            assignedToUserId,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingAssignment> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<TrainingAssignment> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingAssignment? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<TrainingAssignment>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}