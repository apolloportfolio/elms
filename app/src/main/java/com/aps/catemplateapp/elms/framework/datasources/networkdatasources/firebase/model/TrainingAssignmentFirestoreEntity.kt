package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "entities2")
data class TrainingAssignmentFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("name")
    @Expose
    var name: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("ownerID")
    @Expose
    var assignedUserId: UserUniqueID?,

    @SerializedName("title")
    @Expose
    var title: String?,

    @SerializedName("resultsEmail")
    @Expose
    var resultsEmail: String?,

    @SerializedName("picture2URI")
    @Expose
    var picture2URI: String?,

    @SerializedName("assignedToUserId")
    @Expose
    var assignedToUserId: UserUniqueID?,
    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}