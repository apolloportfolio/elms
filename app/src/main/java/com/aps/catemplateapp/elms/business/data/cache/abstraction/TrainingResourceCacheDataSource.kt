package com.aps.catemplateapp.elms.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource

interface TrainingResourceCacheDataSource: StandardCacheDataSource<TrainingResource> {
    override suspend fun insertOrUpdateEntity(entity: TrainingResource): TrainingResource?

    override suspend fun insertEntity(entity: TrainingResource): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<TrainingResource>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        contentType: String?,
        contentUri: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<TrainingResource>

    override suspend fun getAllEntities(): List<TrainingResource>

    override suspend fun getEntityById(id: UniqueID?): TrainingResource?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<TrainingResource>): LongArray
}