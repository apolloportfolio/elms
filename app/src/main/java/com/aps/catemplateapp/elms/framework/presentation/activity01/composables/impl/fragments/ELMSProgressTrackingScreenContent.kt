package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType02
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingAssignmentFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingEventFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsELMS
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

// EmployeeLearningManagementSystemProgressTrackingScreenContent
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ELMSProgressTrackingScreenContent(
    stateEventTracker: StateEventTracker,
    launchInitStateEvent: () -> Unit,

    currentlyShownTrainingCourses: List<TrainingCourse>?,
    currentlyShownTrainingEvents: List<TrainingEvent>?,
    currentlyShownTrainingAssignments: List<TrainingAssignment>?,
    onCourseClick: (TrainingCourse) -> Unit,
    onEventClick: (TrainingEvent) -> Unit,
    onAssignmentClick: (TrainingAssignment) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.elms_app_background,
        composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        val headerBackgroundColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.primaryVariant,
            MaterialTheme.colors.surface,
        )

//        LazyColumn(
//
//        ) {
        LazyColumnWithPullToRefresh(
            onRefresh = launchInitStateEvent,
            isRefreshing = stateEventTracker.isRefreshing,
        ) {
            if(currentlyShownTrainingCourses != null) {
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.training_courses),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeMedium,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.suggested_training_courses)),
                    )
                }
                itemsIndexed(currentlyShownTrainingCourses) { index, course ->
                    ListItemType02(
                        index = index,
                        itemTitleString = course.name,
                        itemDescription = course.description,
                        onListItemClick = { onCourseClick(course) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            course.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }



            if(currentlyShownTrainingEvents != null) {
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.training_events),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeMedium,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.assigned_training_events)),
                    )
                }
                itemsIndexed(currentlyShownTrainingEvents) { index, event ->
                    ListItemType02(
                        index = index,
                        itemTitleString = event.name,
                        itemDescription = event.description,
                        onListItemClick = { onEventClick(event) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            event.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index+2)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }



            if(currentlyShownTrainingAssignments != null) {
                stickyHeader {
                    TitleRowType01(
                        titleString = stringResource(id = R.string.training_assignments),
                        textAlign = TextAlign.Center,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                            colors = headerBackgroundColors,
                            brush = null,
                            shape = null,
                            alpha = 0.7f,
                        ),
                        titleFontSize = Dimens.titleFontSizeMedium,
                        leftImageTint = MaterialTheme.colors.secondary,
                        titleColor = MaterialTheme.colors.secondary,
                        modifier = Modifier
                            .testTag(stringResource(id = R.string.training_assignments)),
                    )
                }
                itemsIndexed(currentlyShownTrainingAssignments) { index, event ->
                    ListItemType02(
                        index = index,
                        itemTitleString = event.title,
                        itemDescription = event.description,
                        onListItemClick = { onAssignmentClick(event) },
                        onItemsButtonClick = {},
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            event.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("elms_example_training_course_", index+5)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }

        }
    }
}


// Preview =========================================================================================
@Composable
@Preview
private fun ELMSProgressTrackingScreenContentPreview() {
    HomeScreenTheme {
        ELMSProgressTrackingScreenContent(
            stateEventTracker = StateEventTracker(),
            launchInitStateEvent = {},
            currentlyShownTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList().take(2),
            currentlyShownTrainingEvents = TrainingEventFactory.createPreviewEntitiesList().take(3),
            currentlyShownTrainingAssignments = TrainingAssignmentFactory.createPreviewEntitiesList().take(2),
            onCourseClick = {},
            onEventClick = {},
            onAssignmentClick = {},
            isPreview = true,
        )
    }
}