package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType02
import com.aps.catemplateapp.common.framework.presentation.views.TagsRowType01
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsELMS
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.Type
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "ELMSCourseDetailsScreen"
private const val LOG_ME = true

@Composable
fun ELMSCourseDetailsScreenContent(
    activity: Activity? = null,
    stateEventTracker: StateEventTracker,
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},
    refreshList: () -> Unit,

    completeUserProfile: () -> Unit,
    currentlyShownTrainingCourse: TrainingCourse?,
    navigateToTrainingResource: (TrainingResource) -> Unit,
    currentlyShownTrainingResources: List<TrainingResource>?,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.elms_app_background,
        composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.spacingLarge)
        ) {
            // Course Title
            val headerBackgroundColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.surface,
            )
            TitleRowType01(
                titleString = currentlyShownTrainingCourse?.name ?: "",
                textAlign = TextAlign.Center,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                    colors = headerBackgroundColors,
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                titleFontSize = Dimens.titleFontSizeMedium,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
                leftPictureDrawableId = null,
                modifier = Modifier.testTag("CourseTitle")
            )

            // Tags
            if(!currentlyShownTrainingCourse?.tags.isNullOrEmpty()) {
                TagsRowType01(
                    tagsInOneString = currentlyShownTrainingCourse!!.tags!!
                )
            }

            // Course Description
            Text(
                text = currentlyShownTrainingCourse?.description ?: "",
                style = Type.body1,
                color = MaterialTheme.colors.onSurface,
                modifier = Modifier
                    .padding(top = Dimens.spacingMedium)
                    .testTag("CourseDescription")
            )

            Spacer(modifier = Modifier.height(12.dp))

            // Other training courses
            TitleRowType01(
                titleString = stringResource(id = R.string.suggested_training_courses),
                textAlign = TextAlign.Center,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsELMS.backgroundTitleType01(
                    colors = null,
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                titleFontSize = Dimens.titleFontSizeSmall,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .testTag(stringResource(id = R.string.suggested_training_courses)),
            )
            // Training Resources List
            if(currentlyShownTrainingResources?.isNotEmpty() == true) {
                LazyColumnWithPullToRefresh(
                    onRefresh = refreshList,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(currentlyShownTrainingResources) { index, item ->
                        ListItemType02(
                            index = index,
                            itemTitleString = item.created_at,
                            itemDescription = item.description,
                            onListItemClick = { navigateToTrainingResource(item) },
                            onItemsButtonClick = {},
                            getItemsRating = { null },
                            itemRef = if(isPreview) {
                                null
                            } else {
                                item.picture1FirebaseImageRef
                            },
                            backgroundDrawableId = R.drawable.example_background_1,
                            composableBackground = BackgroundsOfLayoutsELMS.backgroundListItemType01(
                                colors = null,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("elms_example_training_course_", index)
                            } else { null },
                            imageTint = null,
                            buttonTint = MaterialTheme.colors.secondary,
                            buttonImageVector = null,
                            itemDescriptionFontWeight = FontWeight.Normal,
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.course_details_screen_list_is_empty_tip))
            }
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
private fun ELMSCourseDetailsScreenContentPreview() {
    HomeScreenTheme {
        ELMSCourseDetailsScreenContent(
            activity = null,
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            launchStateEvent = {},
            refreshList = {},

            completeUserProfile = {},
            currentlyShownTrainingCourse = TrainingCourseFactory.createPreviewEntitiesList()[0],
            navigateToTrainingResource = {},
            currentlyShownTrainingResources = TrainingResourceFactory.createPreviewEntitiesList(),

            isPreview = true,
        )
    }
}
