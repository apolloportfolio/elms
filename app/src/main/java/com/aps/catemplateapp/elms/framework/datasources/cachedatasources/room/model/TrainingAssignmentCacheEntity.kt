package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "trainingassignment")
data class TrainingAssignmentCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "name")
    var name : String?,

    @ColumnInfo(name = "assignedUserId")
    var assignedUserId: UserUniqueID?,

    @ColumnInfo(name = "title")
    var title : String?,

    @ColumnInfo(name = "resultsEmail")
    var resultsEmail : String?,

    @ColumnInfo(name = "picture2URI")
    var picture2URI : String?,

    @ColumnInfo(name = "assignedToUserId")
    var assignedToUserId: UserUniqueID?,

    ) {
}