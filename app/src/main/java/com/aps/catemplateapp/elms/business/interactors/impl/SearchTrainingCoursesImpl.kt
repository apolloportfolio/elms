package com.aps.catemplateapp.elms.business.interactors.impl

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingCourseCacheDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingCourseNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.business.interactors.abs.SearchTrainingCourses
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "SearchTrainingCoursesImpl"
private const val LOG_ME = true

class SearchTrainingCoursesImpl
@Inject
constructor(
    private val trainingCourseCacheDataSource: TrainingCourseCacheDataSource,
    private val trainingCourseNetworkDataSource: TrainingCourseNetworkDataSource,
): SearchTrainingCourses {
    override fun searchTrainingCourse(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<TrainingCourse>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeCacheCall(Dispatchers.IO, onErrorAction = onErrorAction){
            if(LOG_ME) ALog.d(TAG, ".searchTrainingCourse(): searchParameters: \n$searchParameters")
            syncEntities(location, searchParameters)
        }


        val response = object: CacheResponseHandler<HomeScreenViewState<ProjectUser>, List<TrainingCourse>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<TrainingCourse>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME)ALog.d(TAG, "searchTrainingCourse().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME)ALog.d(TAG, "searchTrainingCourse(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    if(LOG_ME)ALog.d(TAG, "searchTrainingCourse(): resultObj != null")
                    returnViewState.searchedEntities1List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
        searchParameters : HomeScreenViewState<ProjectUser>,
    ) : List<TrainingCourse> {
        val firestoreSearchParameters = FirestoreTrainingCourseSearchParameters(searchParameters, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters)
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(searchParameters : FirestoreTrainingCourseSearchParameters): List<TrainingCourse>{
        val networkResult = safeApiCall(Dispatchers.IO){
            if(LOG_ME)ALog.d(TAG, ".getNetworkEntities(): searchParameters: \n$searchParameters")
            trainingCourseNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<TrainingCourse>, List<TrainingCourse>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<TrainingCourse>): DataState<List<TrainingCourse>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully searched entities."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}
