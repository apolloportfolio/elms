package com.aps.catemplateapp.elms.di

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingCourseFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingAssignmentFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingResourceFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingEventFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl.TrainingCourseFirestoreServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl.TrainingAssignmentFirestoreServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl.TrainingResourceFirestoreServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl.TrainingEventFirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindTrainingCourseFirestoreService(implementation: TrainingCourseFirestoreServiceImpl): TrainingCourseFirestoreService

    @Binds
    abstract fun bindTrainingAssignmentFirestoreService(implementation: TrainingAssignmentFirestoreServiceImpl): TrainingAssignmentFirestoreService

    @Binds
    abstract fun bindTrainingResourceFirestoreService(implementation: TrainingResourceFirestoreServiceImpl): TrainingResourceFirestoreService

    @Binds
    abstract fun bindTrainingEventFirestoreService(implementation: TrainingEventFirestoreServiceImpl): TrainingEventFirestoreService


}