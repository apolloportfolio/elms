package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingAssignmentDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingAssignmentDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.mappers.TrainingAssignmentCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingAssignmentDaoServiceImpl
@Inject
constructor(
    private val dao: TrainingAssignmentDao,
    private val mapper: TrainingAssignmentCacheMapper,
    private val dateUtil: DateUtil
): TrainingAssignmentDaoService {

    override suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.name,
                entity.description,

                entity.assignedUserId,

                entity.title,
                entity.resultsEmail,
                entity.picture2URI,
                entity.assignedToUserId,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: TrainingAssignment): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<TrainingAssignment>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): TrainingAssignment? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,

        title: String?,
        resultsEmail: String?,
        picture2URI: String?,
        assignedToUserId: UserUniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                name,
                description,

                ownerID,

                title,
                resultsEmail,
                picture2URI,
                assignedToUserId,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                name,
                description,

                ownerID,

                title,
                resultsEmail,
                picture2URI,
                assignedToUserId,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<TrainingAssignment>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<TrainingAssignment> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<TrainingAssignment> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}