package com.aps.catemplateapp.elms.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment

interface TrainingAssignmentNetworkDataSource: StandardNetworkDataSource<TrainingAssignment> {
    override suspend fun insertOrUpdateEntity(entity: TrainingAssignment): TrainingAssignment?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: TrainingAssignment)

    override suspend fun insertDeletedEntities(Entities: List<TrainingAssignment>)

    override suspend fun deleteDeletedEntity(entity: TrainingAssignment)

    override suspend fun getDeletedEntities(): List<TrainingAssignment>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: TrainingAssignment): TrainingAssignment?

    override suspend fun getAllEntities(): List<TrainingAssignment>

    override suspend fun insertOrUpdateEntities(Entities: List<TrainingAssignment>): List<TrainingAssignment>?

    override suspend fun getEntityById(id: UniqueID): TrainingAssignment?

    suspend fun getUsersEntities2(userId : UserUniqueID) : List<TrainingAssignment>?
}