package com.aps.catemplateapp.elms.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingEventNetworkDataSource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingEventFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingEventNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: TrainingEventFirestoreService
): TrainingEventNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: TrainingEvent): TrainingEvent? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: TrainingEvent) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<TrainingEvent>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: TrainingEvent) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<TrainingEvent> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: TrainingEvent): TrainingEvent? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<TrainingEvent> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<TrainingEvent>): List<TrainingEvent>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): TrainingEvent? {
        return firestoreService.getEntityById(id)
    }
}