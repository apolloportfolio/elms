package com.aps.catemplateapp.elms.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingCourseDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingAssignmentDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingResourceDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingEventDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideTrainingCourseDao(database : ProjectRoomDatabase) : TrainingCourseDao {
        return database.trainingCourseDao()
    }

    @Singleton
    @Provides
    fun provideTrainingAssignmentDao(database : ProjectRoomDatabase) : TrainingAssignmentDao {
        return database.trainingAssignmentDao()
    }

    @Singleton
    @Provides
    fun provideTrainingResourceDao(database : ProjectRoomDatabase) : TrainingResourceDao {
        return database.trainingResourceDao()
    }

    @Singleton
    @Provides
    fun provideTrainingEventDao(database : ProjectRoomDatabase) : TrainingEventDao {
        return database.trainingEventDao()
    }
}