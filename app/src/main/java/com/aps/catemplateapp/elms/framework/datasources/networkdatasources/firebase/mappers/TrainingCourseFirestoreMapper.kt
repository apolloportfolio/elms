package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingCourseFirestoreEntity
import javax.inject.Inject

class TrainingCourseFirestoreMapper
@Inject
constructor() : EntityMapper<TrainingCourseFirestoreEntity, TrainingCourse> {
    override fun mapFromEntity(entity: TrainingCourseFirestoreEntity): TrainingCourse {
        val trainingCourse = TrainingCourse(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.lattitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,

            entity.tags,
            entity.trainingResources,
        )
        if(entity.geoLocation != null){
            trainingCourse.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return trainingCourse
    }

    override fun mapToEntity(domainModel: TrainingCourse): TrainingCourseFirestoreEntity {
        val entity1NetworkEntity =
        TrainingCourseFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,
            
            domainModel.city,

            null,           //keywords
            
            domainModel.ownerID,

            domainModel.name,
            
            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,

            domainModel.tags,
            domainModel.trainingResources,
        )
        return entity1NetworkEntity.generateKeywords()
    }

    override fun mapFromEntityList(entities : List<TrainingCourseFirestoreEntity>) : List<TrainingCourse> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<TrainingCourse>): List<TrainingCourseFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}