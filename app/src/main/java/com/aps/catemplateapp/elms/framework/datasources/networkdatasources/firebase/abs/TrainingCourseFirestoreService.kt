package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.interactors.impl.FirestoreTrainingCourseSearchParameters
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingCourseFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingCourseFirestoreEntity

interface TrainingCourseFirestoreService: BasicFirestoreService<
        TrainingCourse,
        TrainingCourseFirestoreEntity,
        TrainingCourseFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreTrainingCourseSearchParameters
    ) : List<TrainingCourse>?

    suspend fun getUsersEntities1(userID: UserUniqueID): List<TrainingCourse>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : TrainingCourse,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}