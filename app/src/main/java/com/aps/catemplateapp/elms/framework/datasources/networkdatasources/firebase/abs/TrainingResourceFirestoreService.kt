package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingResourceFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingResourceFirestoreEntity

interface TrainingResourceFirestoreService: BasicFirestoreService<
        TrainingResource,
        TrainingResourceFirestoreEntity,
        TrainingResourceFirestoreMapper
        > {
    suspend fun getUsersEntities3(userId: UserUniqueID): List<TrainingResource>?

}