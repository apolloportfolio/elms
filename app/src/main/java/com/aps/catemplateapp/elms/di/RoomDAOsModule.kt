package com.aps.catemplateapp.elms.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingCourseDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingAssignmentDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingResourceDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs.TrainingEventDaoService
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl.TrainingCourseDaoServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl.TrainingAssignmentDaoServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl.TrainingResourceDaoServiceImpl
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.impl.TrainingEventDaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindTrainingCourseDaoService(implementation: TrainingCourseDaoServiceImpl): TrainingCourseDaoService

    @Binds
    abstract fun bindTrainingAssignmentDaoService(implementation: TrainingAssignmentDaoServiceImpl): TrainingAssignmentDaoService

    @Binds
    abstract fun bindTrainingResourceDaoService(implementation: TrainingResourceDaoServiceImpl): TrainingResourceDaoService

    @Binds
    abstract fun bindTrainingEventDaoService(implementation: TrainingEventDaoServiceImpl): TrainingEventDaoService

}