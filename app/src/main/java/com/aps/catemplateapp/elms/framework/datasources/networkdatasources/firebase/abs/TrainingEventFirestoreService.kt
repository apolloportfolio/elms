package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingEventFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingEventFirestoreEntity

interface TrainingEventFirestoreService: BasicFirestoreService<
        TrainingEvent,
        TrainingEventFirestoreEntity,
        TrainingEventFirestoreMapper
        > {

}