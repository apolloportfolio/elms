package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse

interface TrainingCourseDaoService {
    suspend fun insertOrUpdateEntity(entity: TrainingCourse): TrainingCourse

    suspend fun insertEntity(entity: TrainingCourse): Long

    suspend fun insertEntities(Entities: List<TrainingCourse>): LongArray

    suspend fun getEntityById(id: UniqueID?): TrainingCourse?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        tags: String?,
        trainingResources: String?
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<TrainingCourse>): Int

    suspend fun searchEntities(): List<TrainingCourse>

    suspend fun getAllEntities(): List<TrainingCourse>

    suspend fun getNumEntities(): Int
}