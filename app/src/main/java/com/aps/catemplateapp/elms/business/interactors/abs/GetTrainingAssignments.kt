package com.aps.catemplateapp.elms.business.interactors.abs

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface GetTrainingAssignments {
    fun getTrainingAssignments(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<TrainingAssignment>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit = {},
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}