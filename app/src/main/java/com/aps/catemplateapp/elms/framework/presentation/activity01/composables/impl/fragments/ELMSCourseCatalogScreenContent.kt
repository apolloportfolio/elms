package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.SearchBarWithClearButton
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.DeviceLocation.Companion.PreviewDeviceLocation
import com.aps.catemplateapp.common.util.GeoLocationUtilities
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsELMS
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

private const val TAG = "ELMSCourseCatalogScreenContent"
private const val LOG_ME = true

// EmployeeLearningManagementSystemCourseCatalogScreenContent
@Composable
fun ELMSCourseCatalogScreenContent(
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    launchInitStateEvent: () -> Unit,

    onTrainingCoursesSearchQueryUpdate: (String) -> Unit,
    onCourseClick: (TrainingCourse) -> Unit,
    currentlyShownTrainingCourses: List<TrainingCourse>?,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    val fragmentsContentDescription = stringResource(id = R.string.home_screen_card1_content_description)
    BoxWithBackground(
        backgroundDrawableId = R.drawable.elms_app_background,
        composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .semantics {
                    contentDescription = fragmentsContentDescription
                },
            verticalArrangement = Arrangement.Center,
        ) {
            var searchQuery by remember { mutableStateOf(initialSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            val searchBarBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )

            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                },
                composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
                    colors = searchBarBackgroundColors,
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                ),
            )

            if(LOG_ME) ALog.d(
                TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME) ALog.d(
                    TAG, "(): " +
                        "Showing profile completion bar")

                val profileStatusBarBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primary,
                )
                ProfileStatusBar(
                    finishVerification,
                    composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
                        colors = profileStatusBarBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    titleTint = MaterialTheme.colors.secondary,
                    subtitleTint = MaterialTheme.colors.error,
                )
            }


            // List of training courses
            val trainingCoursesBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.surface,
                MaterialTheme.colors.surface,
                MaterialTheme.colors.surface,
                MaterialTheme.colors.surface,
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primary,
            )
            if(currentlyShownTrainingCourses?.isNotEmpty() == true) {
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(currentlyShownTrainingCourses) { index, item ->
                        ListItemType03(
                            index = index,
                            itemTitleString = item.name,
                            itemPriceString = "",
                            itemLikesString = "",
                            itemDistanceString = GeoLocationUtilities.distanceFormattedInKM(
                                deviceLocation?.latitude,
                                deviceLocation?.longitude,
                                item.latitude,
                                item.longitude,
                            ),
                            lblSmallShortAttribute3String = "",
                            itemDescriptionString = item.description,
                            onListItemClick = { onCourseClick(item) },
                            getItemsRating = { null },
                            itemRef = if(!isPreview) {
                                item.picture1FirebaseImageRef
                            } else {
                                null
                            },
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("elms_example_training_course_", index)
                            } else { null },
                            imageTint = null,
                            backgroundDrawableId = null,
                            composableBackground = BackgroundsOfLayoutsELMS.backgroundScreen01(
                                colors = trainingCoursesBackgroundColors,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME) ALog.d(
                        TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME) ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onTrainingCoursesSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }
}

//Preview ==========================================================================================
@Preview
@Composable
fun EmployeeLearningManagementSystemCourseCatalogScreenContentPreview1() {
    HomeScreenTheme {
        ELMSCourseCatalogScreenContent(
            stateEventTracker = StateEventTracker(),
            deviceLocation = PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            initialSearchQuery = "",
            launchInitStateEvent = {},
            onTrainingCoursesSearchQueryUpdate = {},
            onCourseClick = {},
            currentlyShownTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList(),

            isPreview = true
        )
    }
}

@Preview
@Composable
fun EmployeeLearningManagementSystemCourseCatalogScreenContentPreview2() {
    HomeScreenTheme {
        ELMSCourseCatalogScreenContent(
            stateEventTracker = StateEventTracker(),
            deviceLocation = PreviewDeviceLocation,
            showProfileStatusBar = true,
            finishVerification = {},
            initialSearchQuery = "",
            launchInitStateEvent = {},
            onTrainingCoursesSearchQueryUpdate = {},
            onCourseClick = {},
            currentlyShownTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList(),

            isPreview = true
        )
    }
}
