package com.aps.catemplateapp.elms.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingCourseCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingAssignmentCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingResourceCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingCourseNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingAssignmentNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingResourceNetworkDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingAssignmentFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.elms.business.interactors.abs.*
import com.aps.catemplateapp.elms.business.interactors.impl.*
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
    ): GetUsersRating {
        return GetUsersRatingImpl(cacheDataSource, networkDataSource)
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetMerchantName {
        return GetMerchantNameImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideDownloadExchangeRates(): DownloadExchangeRates {
        return DownloadExchangeRatesImpl()
    }

    @Provides
    fun provideCheckGooglePayAvailability(): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl()
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: TrainingCourseCacheDataSource,
        networkDataSource: TrainingCourseNetworkDataSource,
        entityFactory: TrainingCourseFactory
    ): GetTrainingCoursesAroundUser {
        return GetTrainingCoursesAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: TrainingAssignmentCacheDataSource,
        networkDataSource: TrainingAssignmentNetworkDataSource,
        entityFactory: TrainingAssignmentFactory
    ): GetTrainingAssignments {
        return GetTrainingAssignmentsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: TrainingResourceCacheDataSource,
        networkDataSource: TrainingResourceNetworkDataSource,
        entityFactory: TrainingResourceFactory
    ): GetTrainingResources {
        return GetTrainingResourcesImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideSearchEntities1(
        cacheDataSource: TrainingCourseCacheDataSource,
        networkDataSource: TrainingCourseNetworkDataSource,
        entityFactory: TrainingCourseFactory
    ): SearchTrainingCourses {
        return SearchTrainingCoursesImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}