package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingAssignmentFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingAssignmentFirestoreEntity

interface TrainingAssignmentFirestoreService: BasicFirestoreService<
        TrainingAssignment,
        TrainingAssignmentFirestoreEntity,
        TrainingAssignmentFirestoreMapper
        > {
    suspend fun getUsersEntities2(userId: UserUniqueID): List<TrainingAssignment>?

}