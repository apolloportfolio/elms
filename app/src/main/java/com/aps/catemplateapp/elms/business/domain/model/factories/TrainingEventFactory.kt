package com.aps.catemplateapp.elms.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingEventFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,

        date: String?,
        time: String?,
    ): TrainingEvent {
        return TrainingEvent(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,

            date,
            time,
        )
    }

    fun createEntitiesList(numEntities: Int): List<TrainingEvent> {
        val list: ArrayList<TrainingEvent> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): TrainingEvent {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<TrainingEvent> {
            return arrayListOf(
                TrainingEvent(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = ParcelableGeoPoint(54.23154, 21.614117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Join our intensive coding bootcamp and learn full-stack web development in just 12 weeks. Gain hands-on experience with HTML, CSS, JavaScript, Node.js, and React, and build real-world projects.",
                    name = "Full-Stack Web Development Bootcamp",
                    date = "2022-01-04",
                    time = "15:30:00"
                ),
                TrainingEvent(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = ParcelableGeoPoint(54.23154, 21.614117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Experience an immersive culinary journey with our gourmet cooking workshop. Learn advanced cooking techniques, flavor pairing, and plating presentation from renowned chefs. ",
                    name = "Gourmet Cooking Workshop",
                    date = "2022-01-05",
                    time = "16:00:00"
                ),
                TrainingEvent(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = ParcelableGeoPoint(54.23154, 21.614117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Attend our one-day digital marketing summit and gain insights from industry experts on the latest trends, strategies, and tools to elevate your online marketing efforts. ",
                    name = "Digital Marketing Summit",
                    date = "2022-01-06",
                    time = "17:30:00"
                ),
                TrainingEvent(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = ParcelableGeoPoint(51.23164, 22.614167),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Participate in our leadership conference and discover actionable strategies to enhance your leadership skills. Learn from top executives and thought leaders in various industries. ",
                    name = "Leadership Conference",
                    date = "2022-01-07",
                    time = "18:45:00"
                ),
                TrainingEvent(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = ParcelableGeoPoint(51.23254, 22.615117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Explore the future of AI and machine learning at our technology symposium. Hear from leading researchers and industry pioneers about the latest advancements and applications. ",
                    name = "AI & Machine Learning Symposium",
                    date = "2022-01-08",
                    time = "09:15:00"
                ),
                TrainingEvent(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = ParcelableGeoPoint(51.23354, 22.614717),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Join our entrepreneurship workshop and learn how to launch and grow a successful startup. Gain practical insights into fundraising, product development, and market strategies. ",
                    name = "Entrepreneurship Workshop",
                    date = "2022-01-09",
                    time = "10:30:00"
                ),
                TrainingEvent(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = ParcelableGeoPoint(51.23154, 22.624117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Experience the thrill of adventure photography with our outdoor photography expedition. Capture stunning landscapes and learn photography techniques from seasoned professionals. ",
                    name = "Adventure Photography Expedition",
                    date = "2022-01-10",
                    time = "12:00:00"
                ),
                TrainingEvent(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = ParcelableGeoPoint(51.23154, 22.664117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Attend our wellness retreat and rejuvenate your mind, body, and spirit. Participate in yoga sessions, meditation workshops, and holistic wellness activities in a serene environment. ",
                    name = "Wellness Retreat",
                    date = "2022-01-11",
                    time = "14:45:00"
                ),
                TrainingEvent(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = ParcelableGeoPoint(51.23154, 22.6147),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Join our finance seminar and gain valuable insights into personal finance, investment strategies, and wealth management. Learn from financial experts and industry insiders. ",
                    name = "Finance Seminar",
                    date = "2022-01-12",
                    time = "16:20:00"
                ),
                TrainingEvent(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = ParcelableGeoPoint(51.23154, 22.714117),
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Participate in our diversity and inclusion workshop and learn practical strategies to create an inclusive workplace culture. Gain insights into unconscious bias, allyship, and diversity initiatives. ",
                    name = "Diversity & Inclusion Workshop",
                    date = "2022-01-13",
                    time = "18:00:00"
                )
            )
        }

    }
}