package com.aps.catemplateapp.elms.di

import com.aps.catemplateapp.elms.business.data.network.abs.TrainingCourseNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingAssignmentNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingResourceNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.abs.TrainingEventNetworkDataSource
import com.aps.catemplateapp.elms.business.data.network.impl.TrainingCourseNetworkDataSourceImpl
import com.aps.catemplateapp.elms.business.data.network.impl.TrainingAssignmentNetworkDataSourceImpl
import com.aps.catemplateapp.elms.business.data.network.impl.TrainingResourceNetworkDataSourceImpl
import com.aps.catemplateapp.elms.business.data.network.impl.TrainingEventNetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindTrainingCourseNetworkDataSource(implementation: TrainingCourseNetworkDataSourceImpl): TrainingCourseNetworkDataSource

    @Binds
    abstract fun bindTrainingAssignmentNetworkDataSource(implementation: TrainingAssignmentNetworkDataSourceImpl): TrainingAssignmentNetworkDataSource

    @Binds
    abstract fun bindTrainingResourceNetworkDataSource(implementation: TrainingResourceNetworkDataSourceImpl): TrainingResourceNetworkDataSource

    @Binds
    abstract fun bindTrainingEventNetworkDataSource(implementation: TrainingEventNetworkDataSourceImpl): TrainingEventNetworkDataSource

}