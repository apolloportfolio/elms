package com.aps.catemplateapp.elms.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrainingResourceFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        description : String?,

        ownerID: UserUniqueID?,
        
        title: String?,
        contentType: String?,
        contentUri: String?,
    ): TrainingResource {
        return TrainingResource(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            description,

            ownerID,

            title,
            contentType,
            contentUri,
        )
    }

    fun createEntitiesList(numEntities: Int): List<TrainingResource> {
        val list: ArrayList<TrainingResource> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): TrainingResource {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<TrainingResource> {
            return arrayListOf(
                TrainingResource(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "A comprehensive guide to Python programming for beginners, covering basic syntax, data types, control structures, and functions. Includes hands-on exercises and quizzes to reinforce learning.",
                    title = "Python Programming for Beginners",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 1",
                    contentUri = "http://example.com/content1"
                ),
                TrainingResource(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Learn the fundamentals of web development with HTML, CSS, and JavaScript. Build and style your own web pages from scratch, and understand how to add interactivity with JavaScript.",
                    title = "Introduction to Web Development",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 2",
                    contentUri = "http://example.com/content2"
                ),
                TrainingResource(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Master the art of digital photography with this course, covering camera settings, composition techniques, and editing basics using Adobe Photoshop. Suitable for beginners and enthusiasts.",
                    title = "Digital Photography Essentials",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 3",
                    contentUri = "http://example.com/content3"
                ),
                TrainingResource(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "An in-depth guide to financial analysis and modeling using Microsoft Excel. Learn to create financial models, perform valuation, and analyze company performance using real-world data.",
                    title = "Financial Modeling with Excel",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 4",
                    contentUri = "http://example.com/content4"
                ),
                TrainingResource(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Unlock the power of data analysis with Python and Pandas. Explore data manipulation, cleaning, and visualization techniques to extract insights and make informed decisions.",
                    title = "Data Analysis with Python and Pandas",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 5",
                    contentUri = "http://example.com/content5"
                ),
                TrainingResource(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Develop your graphic design skills with Adobe Illustrator. Learn to create vector graphics, logos, and illustrations from scratch, and understand fundamental design principles.",
                    title = "Graphic Design with Adobe Illustrator",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 6",
                    contentUri = "http://example.com/content6"
                ),
                TrainingResource(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Discover the principles of user experience (UX) design and learn to create intuitive, user-friendly interfaces. Gain practical skills in wireframing, prototyping, and usability testing.",
                    title = "Introduction to UX Design",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 7",
                    contentUri = "http://example.com/content7"
                ),
                TrainingResource(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Explore the world of artificial intelligence and machine learning. Understand core concepts, algorithms, and applications, and learn to build and train your own machine learning models.",
                    title = "Introduction to Machine Learning",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 8",
                    contentUri = "http://example.com/content8"
                ),
                TrainingResource(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Enhance your communication skills with this comprehensive public speaking course. Learn techniques to overcome stage fright, structure effective speeches, and engage your audience.",
                    title = "Mastering Public Speaking",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 9",
                    contentUri = "http://example.com/content9"
                ),
                TrainingResource(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Improve your productivity and time management with this practical course. Learn strategies for setting goals, prioritizing tasks, and minimizing distractions to achieve more in less time.",
                    title = "Productivity Hacks for Professionals",
                    ownerID = UserUniqueID("666", "666"),
                    contentType = "Example Content Type 10",
                    contentUri = "http://example.com/content10"
                )
            )
        }

    }
}