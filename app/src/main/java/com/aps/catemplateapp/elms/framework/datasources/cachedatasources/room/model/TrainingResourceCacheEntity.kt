package com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "trainingresource")
data class TrainingResourceCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,

    @ColumnInfo(name = "title")
    var title : String?,

    @ColumnInfo(name = "contentType")
    var contentType : String?,

    @ColumnInfo(name = "contentUri")
    var contentUri : String?,

    ) {
}