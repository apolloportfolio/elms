package com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.business.data.util.cLog
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.common.util.extensions.getSizeString
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.abs.TrainingResourceFirestoreService
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.mappers.TrainingResourceFirestoreMapper
import com.aps.catemplateapp.elms.framework.datasources.networkdatasources.firebase.model.TrainingResourceFirestoreEntity
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TrainingResourceFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: TrainingResourceFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): TrainingResourceFirestoreService,
    BasicFirestoreServiceImpl<TrainingResource, TrainingResourceFirestoreEntity, TrainingResourceFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: TrainingResource, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: TrainingResource): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: TrainingResourceFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: TrainingResource, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: TrainingResourceFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<TrainingResourceFirestoreEntity> {
        return TrainingResourceFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: TrainingResourceFirestoreEntity): UniqueID? {
        return entity.id
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<TrainingResource>? {
        val methodName: String = "getUsersEntities3"
        var result: List<TrainingResource>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(getCollectionName())
                    .whereEqualTo(TrainingResource.ownerIDFieldName, userId)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(
                            TAG, "$methodName(): " +
                                "Failed to get user's entities3.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Successfully got all user's entities3.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "Got ${result.getSizeString()} entities3 " +
                    "belonging to user: $userId stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "Entity3FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity3"
        const val DELETES_COLLECTION_NAME = "entity3_d"
    }
}