package com.aps.catemplateapp.elms.di

import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.cache.implementation.*
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingCourseCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingAssignmentCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingResourceCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.abstraction.TrainingEventCacheDataSource
import com.aps.catemplateapp.elms.business.data.cache.implementation.TrainingCourseCacheDataSourceImpl
import com.aps.catemplateapp.elms.business.data.cache.implementation.TrainingAssignmentCacheDataSourceImpl
import com.aps.catemplateapp.elms.business.data.cache.implementation.TrainingResourceCacheDataSourceImpl
import com.aps.catemplateapp.elms.business.data.cache.implementation.TrainingEventCacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindTrainingCourseCacheDataSource(implementation: TrainingCourseCacheDataSourceImpl): TrainingCourseCacheDataSource

    @Binds
    abstract fun bindTrainingAssignmentCacheDataSource(implementation: TrainingAssignmentCacheDataSourceImpl): TrainingAssignmentCacheDataSource

    @Binds
    abstract fun bindTrainingResourceCacheDataSource(implementation: TrainingResourceCacheDataSourceImpl): TrainingResourceCacheDataSource

    @Binds
    abstract fun bindTrainingEventCacheDataSource(implementation: TrainingEventCacheDataSourceImpl): TrainingEventCacheDataSource
}