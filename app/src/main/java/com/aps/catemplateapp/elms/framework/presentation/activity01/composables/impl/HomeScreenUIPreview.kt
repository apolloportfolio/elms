package com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl

import android.location.Location
import android.widget.Toast
import androidx.compose.material.SnackbarDuration
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingCourse
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingAssignment
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingResource
import com.aps.catemplateapp.elms.business.domain.model.entities.TrainingEvent
import com.aps.catemplateapp.elms.business.domain.model.entities.UserTrainingProgress
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingCourseFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingAssignmentFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingResourceFactory
import com.aps.catemplateapp.elms.business.domain.model.factories.TrainingEventFactory
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment1BottomSheetActions
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment2BottomSheetActions
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment3BottomSheetActions
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment4BottomSheetActions
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment5BottomSheetActions
import com.aps.catemplateapp.elms.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.elms.framework.presentation.activity01.state.HomeScreenCard1SearchFilters

@Preview(
    showSystemUi = true,
//    device = "spec:width=411dp,height=891dp,dpi=420,isRound=false,chinSize=0dp,orientation=landscape"
//            device = "spec:width=411dp,height=891dp,dpi=420,isRound=false,chinSize=0dp,orientation=portrait"
//    device = Devices.AUTOMOTIVE_1024p, widthDp = 1024, heightDp = 720,
)
@Composable
fun HomeScreenUIPreview(
    @PreviewParameter(HomeScreenUIParamsProvider::class) params: HomeScreenUIParams
) {
    // Note: Start Interactive Mode to actually see SnackBar
    HomeScreenTheme {
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val trainingEvents = TrainingEventFactory.createPreviewEntitiesList()
        val userTrainingProgress = UserTrainingProgress(
            id = UniqueID.generateFakeRandomID(),
            created_at = DateUtil.getPastTimestampAsStringForPreview(),
            updated_at = DateUtil.getPastTimestampAsStringForPreview(),
            userId = UserUniqueID.generateFakeRandomID(),
            consumedResources = TrainingResourceFactory.createPreviewEntitiesList(),
            consumedEvents = trainingEvents.subList(0, trainingEvents.size/2),
            assignedCourses = TrainingCourseFactory.createPreviewEntitiesList(),
            assignedEvents = trainingEvents.subList(
                trainingEvents.size/2,
                trainingEvents.size.coerceAtMost(trainingEvents.size),
            ),
        )

        HomeScreenUI(
            suggestedTrainingCourses = TrainingCourseFactory.createPreviewEntitiesList(),
            userTrainingProgress = userTrainingProgress,
            currentlyShownTrainingCourse = null,
            currentlyShownTrainingResources = null,
            currentlyShownTrainingCourses = null,
            currentlyShownTrainingEvents = null,
            currentlyShownTrainingAssignments = null,
            onTrainingCoursesSearchQueryUpdate = {},

            activity = null,
            permissionHandlingData = PermissionHandlingData(),
            stateEventTracker = StateEventTracker(),

            deviceLocation = params.deviceLocation,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            isDrawerOpen = null,
            updateOrientation = params.updateOrientation,
            leftPaneStartDestination = params.leftPaneStartDestination,
            rightPaneStartDestination = params.rightPaneStartDestination,
            showProfileStatusBar = params.showProfileStatusBar,
            card1SearchQuery = params.card1SearchQuery,
            card1OnSearchQueryUpdate = params.card1OnSearchQueryUpdate,
            searchFilters = params.searchFilters,
            onSearchFiltersUpdated = params.onSearchFiltersUpdated,
            floatingActionButtonDrawableIdCard1 = params.floatingActionButtonDrawableIdCard1,
            floatingActionButtonOnClickCard1 = params.floatingActionButtonOnClickCard1,
            floatingActionButtonContentDescriptionCard1 = params.floatingActionButtonContentDescriptionCard1,
            card1EntitiesList = params.card1EntitiesList,
            card1OnListItemClick = params.card1OnListItemClick,
            floatingActionButtonDrawableIdCard2 = params.floatingActionButtonDrawableIdCard2,
            floatingActionButtonOnClickCard2 = params.floatingActionButtonOnClickCard2,
            floatingActionButtonContentDescriptionCard2 = params.floatingActionButtonContentDescriptionCard2,
            card1BottomSheetActions = HomeScreenComposableFragment1BottomSheetActions(
                leftButtonOnClick = {},
                rightButtonOnClick = {},
            ),
            card1ActionOnEntity = { _, _ -> },
            card2EntitiesList = params.card2EntitiesList,
            card2OnListItemClick = params.card2OnListItemClick,
            card2BottomSheetActions = HomeScreenComposableFragment2BottomSheetActions(
                leftButtonOnClick = {},
                rightButtonOnClick = {},
            ),
            floatingActionButtonDrawableIdCard3 = params.floatingActionButtonDrawableIdCard3,
            floatingActionButtonOnClickCard3 = params.floatingActionButtonOnClickCard3,
            floatingActionButtonContentDescriptionCard3 = params.floatingActionButtonContentDescriptionCard3,
            card3EntitiesList = params.card3EntitiesList,
            card3OnListItemClick = params.card3OnListItemClick,
            card3BottomSheetActions = HomeScreenComposableFragment3BottomSheetActions(
                leftButtonOnClick = {},
                rightButtonOnClick = {},
            ),
            floatingActionButtonDrawableIdCard4 = params.floatingActionButtonDrawableIdCard4,
            floatingActionButtonOnClickCard4 = params.floatingActionButtonOnClickCard4,
            floatingActionButtonContentDescriptionCard4 = params.floatingActionButtonContentDescriptionCard4,
            card4EntitiesList = params.card4EntitiesList,
            card4OnListItemClick = params.card4OnListItemClick,
            card4BottomSheetActions = HomeScreenComposableFragment4BottomSheetActions(
                leftButtonOnClick = {},
                rightButtonOnClick = {},
            ),
            card5BottomSheetActions = HomeScreenComposableFragment5BottomSheetActions(
                leftButtonOnClick = {},
                rightButtonOnClick = {},
            ),
            isPreview = true,
        )
    }

}

class HomeScreenUIParamsProvider : PreviewParameterProvider<HomeScreenUIParams> {

//    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
//    private val context: Context = ProjectApplication.applicationContext("HomeScreenUIParamsProvider")

    override val values: Sequence<HomeScreenUIParams> = sequenceOf(
        HomeScreenUIParams(
            deviceLocation = DeviceLocation(
                locationPermissionGranted = true,
                location = Location("Lublin").apply {
                    latitude = 51.2465
                    longitude = 22.5684
                }
            ),
            showSnackbar = null,
            updateOrientation = { },
            leftPaneStartDestination = HomeScreenDestination.Card1.route,
            rightPaneStartDestination = HomeScreenDestination.DetailsPlaceholderScreen.route,
            showProfileStatusBar = false,
            card1SearchQuery = "",
            card1OnSearchQueryUpdate = {  },
            searchFilters = HomeScreenCard1SearchFilters(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ),
            onSearchFiltersUpdated = {  },
            floatingActionButtonDrawableIdCard1 = null,
            floatingActionButtonOnClickCard1 = null,
            floatingActionButtonContentDescriptionCard1 = null,
            card1EntitiesList = TrainingCourseFactory.createPreviewEntitiesList(),
            card1OnListItemClick = {  },
            floatingActionButtonDrawableIdCard2 = R.drawable.ic_search_yellow_24,
            floatingActionButtonOnClickCard2 = null,
            floatingActionButtonContentDescriptionCard2 = "FAB 2",
            card2EntitiesList = TrainingAssignmentFactory.createPreviewEntitiesList(),
            card2OnListItemClick = {  },
            floatingActionButtonDrawableIdCard3 = R.drawable.ic_baseline_add_24,
            floatingActionButtonOnClickCard3 = null,
            floatingActionButtonContentDescriptionCard3 = "FAB 3",
            card3EntitiesList = TrainingResourceFactory.createPreviewEntitiesList(),
            card3OnListItemClick = {  },
            floatingActionButtonDrawableIdCard4 = R.drawable.ic_baseline_add_24,
            floatingActionButtonOnClickCard4 = null,
            floatingActionButtonContentDescriptionCard4 = "FAB 4",
            card4EntitiesList = TrainingEventFactory.createPreviewEntitiesList(),
            card4OnListItemClick = {  }
        )
    )

    override val count: Int = values.count()
}

class HomeScreenUIParams(
    val deviceLocation: DeviceLocation? = null,
    val showSnackbar: SnackBarState? = null,
    val updateOrientation: (Boolean) -> Unit = {},
    val leftPaneStartDestination: String = "",
    val rightPaneStartDestination: String = "",
    val showProfileStatusBar: Boolean = false,
    val card1SearchQuery: String = "",
    val card1OnSearchQueryUpdate: (String) -> Unit = {},
    val searchFilters: HomeScreenCard1SearchFilters = HomeScreenCard1SearchFilters(),
    val onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit = {},
    val floatingActionButtonDrawableIdCard1: Int? = null,
    val floatingActionButtonOnClickCard1: (() -> Unit)? = null,
    val floatingActionButtonContentDescriptionCard1: String? = null,
    val card1EntitiesList: List<TrainingCourse>? = null,
    val card1OnListItemClick: (TrainingCourse) -> Unit = {},
    val floatingActionButtonDrawableIdCard2: Int? = null,
    val floatingActionButtonOnClickCard2: (() -> Unit)? = null,
    val floatingActionButtonContentDescriptionCard2: String? = null,
    val card2EntitiesList: List<TrainingAssignment>? = null,
    val card2OnListItemClick: (TrainingAssignment) -> Unit = {},
    val floatingActionButtonDrawableIdCard3: Int? = null,
    val floatingActionButtonOnClickCard3: (() -> Unit)? = null,
    val floatingActionButtonContentDescriptionCard3: String? = null,
    val card3EntitiesList: List<TrainingResource>? = null,
    val card3OnListItemClick: (TrainingResource) -> Unit = {},
    val floatingActionButtonDrawableIdCard4: Int? = null,
    val floatingActionButtonOnClickCard4: (() -> Unit)? = null,
    val floatingActionButtonContentDescriptionCard4: String? = null,
    val card4EntitiesList: List<TrainingEvent>? = null,
    val card4OnListItemClick: (TrainingEvent) -> Unit = {}
)