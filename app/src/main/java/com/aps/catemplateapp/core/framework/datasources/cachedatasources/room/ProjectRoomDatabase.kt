package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.*
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingAssignmentDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingCourseDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingEventDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.daos.TrainingResourceDao
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingAssignmentCacheEntity
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingCourseCacheEntity
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingEventCacheEntity
import com.aps.catemplateapp.elms.framework.datasources.cachedatasources.room.model.TrainingResourceCacheEntity

@Database(entities = [
    UserCacheEntity::class,

    TrainingCourseCacheEntity::class,
    TrainingAssignmentCacheEntity::class,
    TrainingResourceCacheEntity::class,
    TrainingEventCacheEntity::class,
], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // Employee Learning Management System
    abstract fun trainingCourseDao() : TrainingCourseDao

    abstract fun trainingAssignmentDao() : TrainingAssignmentDao

    abstract fun trainingResourceDao() : TrainingResourceDao

    abstract fun trainingEventDao() : TrainingEventDao


    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}