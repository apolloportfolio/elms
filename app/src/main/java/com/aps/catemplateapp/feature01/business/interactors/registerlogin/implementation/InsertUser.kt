package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.InsertUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class InsertUserImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val entityFactory: UserFactory
): InsertUser {

    override fun insertNewEntity(
        newEntity: ProjectUser,
        stateEvent: StateEvent
    ):Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val registerResult = safeApiCall(Dispatchers.IO){
            networkDataSource.insertOrUpdateEntity(newEntity)
        }

        val cacheResponse = object: ApiResponseHandler<RegisterLoginActivityViewState<ProjectUser>, ProjectUser?>(
            response = registerResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: ProjectUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            appProjectUser = resultObj
                        )
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = INSERT_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                }
                else{
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = INSERT_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(cacheResponse)
    }

    companion object{
        val INSERT_ENTITY_SUCCESS = "Successfully inserted new entity."
        val INSERT_ENTITY_FAILED = "Failed to insert new entity."
    }
}