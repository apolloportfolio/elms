package com.aps.catemplateapp.common.framework.presentation.views.appbar

enum class ShowAppBarItemAsAction {
    NEVER_OVERFLOW, IF_NECESSARY, ALWAYS_OVERFLOW, NOT_SHOWN
}