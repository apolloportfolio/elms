package com.aps.catemplateapp.common.util

// simple callback to execute something after a function is called
interface TodoCallback {

    fun execute()
}